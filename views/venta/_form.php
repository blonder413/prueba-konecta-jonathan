<?php

use app\models\Producto;
use kartik\select2\Select2;
use yii\helpers\{ArrayHelper, Html};
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Venta */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="venta-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'producto_id')->widget(Select2::class, [
        'data' => ArrayHelper::map(Producto::find()->orderBy('nombre asc')->all(), 'id', 'nombre'),
        'language' => 'es',
        'options' => ['placeholder' => 'Producto ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'cantidad')->textInput(['type' => 'number']) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
