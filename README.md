<p align="center">
    <a href="https://github.com/yiisoft" target="_blank">
        <img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
    </a>
    <h1 align="center">Prueba Konecta. Template básico Yii 2</h1>
    <br>
</p>

ESTRUCTURA DE DIRECTORIOS
-------------------------

      assets/             contiene definiciones de assets
      commands/           contiene comandos de consola (controladores)
      config/             contiene configuraciones de la aplicación
      controllers/        contiene clases web, controladores
      mail/               contiene archivos de vistas para correos electrónicos
      migrations/         contiene los archivos de migración para crear la base de datos
      models/             contiene clases de los modelos
      runtime/            contiene archivos generados durante la ejecución
      tests/              contiene varias pruebas
      vendor/             contiene paquetes de terceros
      views/              contiene archivos de vistas para la aplicación
      web/                contiene scripts y recursos web



REQUISITOS
------------

Los requerimientos mínimos son PHP 5.6.0. Puede ver la lista completa de requisitos en
~~~
http://localhost/prueba-konecta-jonathan/requirements.php
~~~

INSTALACIÓN
------------

si no tiene [Composer](http://getcomposer.org/), puede instalarlo siguiendo las instrucciones
en [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

si no tiene [Git](https://git-scm.com/), puede descargarlo desde
[la web oficial](https://git-scm.com/downloads).

Primero debe descargar el proyecto con git

~~~
git clone https://gitlab.com/blonder413/prueba-konecta-jonathan.git
~~~

Después debe instalar las extensiones

~~~
cd prueba-konecta-jonathan
composer install
~~~

Luego debe configurar la base de datos como dice el apartado configuración.

A continuación debe crear las tablas corriendo la migración

~~~
php yii migrate
~~~

Finalmente podrá acceder a la aplicación a través del navegador web

~~~
http://localhost/prueba-konecta-jonathan/web/
~~~

Opiconalmente puede ejecutar el seeder para tener datos de prueba. La configuración se encuentra en commands\SeedController

```
php yii seed
```

CONFIGURACIÓN
-------------

### Base de datos

Copiar el archivo `config/db-example.php` como `config/db.php` y modificarlo con datos reales, por ejemplo:

```php
return [
    'class' => 'yii\db\Connection',
    'driverName'    => 'mysql',
    'dsn' => 'mysql:host=localhost;dbname=yii2basic',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```

### Params

Copiar el archivo `config/params-example.php` como `config/params.php` y modificarlo con datos reales, por ejemplo:

```php
return [
    'adminEmail' => 'admin@grupokonecta.com',
    'senderEmail' => 'noreply@grupokonecta.com',
    'senderName' => 'GrupoKonecta.com mailer',
];
```

# Extensiones

- yii2-grid ( https://demos.krajee.com/grid )

```
composer require kartik-v/yii2-grid "dev-master"
```

- yii2-mpdf ( https://demos.krajee.com/mpdf )

```
composer require kartik-v/yii2-mpdf "dev-master"
```

- kartik-v/yii2-bootstrap4-dropdown (https://demos.krajee.com/bs4/bootstrap4-dropdown)

```
composer require kartik-v/yii2-bootstrap4-dropdown "dev-master"

```