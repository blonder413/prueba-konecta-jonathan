<?php

namespace app\models;

use \yii\db\Expression;
use \yii\behaviors\TimestampBehavior;
use Yii;

/**
 * This is the model class for table "producto".
 *
 * @property int $id
 * @property string $nombre
 * @property string $referencia
 * @property int $precio
 * @property int $peso
 * @property string $categoria
 * @property int $stock
 * @property string $fecha_creacion
 */
class Producto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'producto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'referencia', 'precio', 'peso', 'categoria', 'stock'], 'required'],
            [['precio', 'peso', 'stock'], 'integer'],
            [['fecha_creacion'], 'safe'],
            [['nombre', 'categoria'], 'string', 'max' => 255],
            [['referencia'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'referencia' => 'Referencia',
            'precio' => 'Precio',
            'peso' => 'Peso',
            'categoria' => 'Categoría',
            'stock' => 'Stock',
            'fecha_creacion' => 'Fecha Creación',
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'fecha_creacion',
                'updatedAtAttribute' => null,
                'value' => new Expression('NOW()'),
            ],
        ];
    }
}
