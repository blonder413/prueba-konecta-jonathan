<?php

namespace app\models;
use app\models\Producto;
use Yii;

/**
 * This is the model class for table "venta".
 *
 * @property int $id
 * @property int $producto_id
 * @property int $cantidad
 *
 * @property Producto $producto
 */
class Venta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'venta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['producto_id', 'cantidad'], 'required'],
            [['producto_id', 'cantidad'], 'integer'],
            [['cantidad'], 'validarStock'],
            [['producto_id'], 'exist', 'skipOnError' => true, 'targetClass' => Producto::className(), 'targetAttribute' => ['producto_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'producto_id' => 'Producto',
            'cantidad' => 'Cantidad',
        ];
    }

    /**
     * Gets query for [[Producto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProducto()
    {
        return $this->hasOne(Producto::className(), ['id' => 'producto_id']);
    }

    /*
     * Reglas
     */
    public function validarStock()
    {
        $producto = Producto::findOne($this->producto_id);

        if ($producto->stock < $this->cantidad) {
            $this->addError('cantidad', 'No hay stock suficiente');
        }
    }
}
