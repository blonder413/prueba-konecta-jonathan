<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'bsVersion' => '5.x', // Establece `bsVersion` a Bootstrap 5.x para todas las extensiones Krajee
];
