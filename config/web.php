<?php
use kartik\mpdf\Pdf;

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language'  => 'es',
    'timeZone'          => 'America/Bogota',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'XtYwn-NU8on8VOyStknkxHP38FvqwLOV',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'formatter' => [
            'class' => '\yii\i18n\Formatter',
            'defaultTimeZone'   => 'America/Bogota',
//            'dateFormat' => 'dd.MM.yyyy',
            'datetimeFormat'    => 'php:D j \d\e M \d\e Y g:i a',
            'locale'    => 'es_ES',
//            'language'  => 'es',
            'thousandSeparator' => '.',
            'decimalSeparator' => ',',
            'currencyCode' => '$'
        ],
        // https://demos.krajee.com/mpdf
        // setup Krajee Pdf component
        'pdf' => [
            'class'             => Pdf::classname(),
            'format'            => Pdf::FORMAT_LETTER,
            'mode'              => Pdf::MODE_CORE, 
            'orientation'       => Pdf::ORIENT_PORTRAIT,
            'destination'       => Pdf::DEST_BROWSER,
//            'cssFile'           => 'css/azul/pdf.css',
            'defaultFont'       => 'Roboto',
//            'marginTop'         => 13,
//            'marginBottom'      => 13,
//            'marginLeft'        => 13,
//            'marginRight'       => 13,
            'options'   => [
                'showWatermarkText' => true,
                'showWatermarkImage' => true,
            ],
            
            'defaultFontSize'   => 12,
            // refer settings section for all configuration options
        ],
        'db' => $db,
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'modules' => [
        'gridview' =>  [
             'class' => '\kartik\grid\Module'
             // enter optional module parameters below - only if you need to  
             // use your own export download action or custom translation 
             // message source
             // 'downloadAction' => 'gridview/export/download',
             // 'i18n' => []
        ]
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
        'generators' => [ // HERE
            'crud' => [
                'class' => 'yii\gii\generators\crud\Generator',
                'templates' => [
                    'adminlte' => '@vendor/dmstr/yii2-adminlte-asset/gii/templates/crud/simple',
                    'miCrud' => '@app/misTemplates/crud/default',
                ]
            ]
        ],
    ];
}

return $config;
