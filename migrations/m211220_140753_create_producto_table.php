<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%producto}}`.
 */
class m211220_140753_create_producto_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_spanish_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%producto}}', [
            'id'                => $this->primaryKey(),
            'nombre'            => $this->string(255)->notNull(),
            'referencia'        => $this->string(50)->notNull(),
            'precio'            => $this->integer()->notNull(),
            'peso'              => $this->integer()->notNull(),
            'categoria'         => $this->string(255)->notNull(),
            'stock'             => $this->integer()->notNull(),
            'fecha_creacion'    => $this->dateTime()->notNull(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%producto}}');
    }
}
