<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%venta}}`.
 */
class m211220_160533_create_venta_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_spanish_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%venta}}', [
            'id' => $this->primaryKey(),
            'producto_id'   => $this->integer()->notNull(),
            'cantidad'      => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addForeignKey(
            'productoventa', 'venta', 'producto_id', 'producto', 'id', 'no action', 'no action'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('productoventa', 'venta');
        $this->dropTable('{{%venta}}');
    }
}
