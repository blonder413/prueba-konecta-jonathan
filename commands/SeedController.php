<?php

// commands/SeedController.php

namespace app\commands;

use app\models\Seguridad;
use yii\console\Controller;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\Expression;
use yii\db\QueryBuilder;
use yii\helpers\Console;


class SeedController extends Controller {
    public function actionIndex()
    {
        $faker = \Faker\Factory::create('es_ES');
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $this->actionProducto();

            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * seeder para la tabla producto
     */
    public function actionProducto()
    {
        $faker = \Faker\Factory::create('es_ES');

        $this->stdout("insertando registros en la tabla producto\n", Console::FG_YELLOW);
        for ($i = 0; $i < 50; $i++) {
            Yii::$app->db->createCommand()->batchInsert('producto',
                    [
                        'nombre', 'referencia', 'precio', 'peso',
                        'categoria', 'stock', 'fecha_creacion'
                    ],
                    [
                        [
                            $faker->unique()->word,
                            $faker->unique()->word,
                            $faker->numberBetween(1000, 50000),
                            $faker->numberBetween(1, 50),
                            $faker->text(100),
                            $faker->numberBetween(1, 50),
                            new Expression('NOW()'),
                        ],
                    ]
            )->execute();
//            $this->stdout("Producto insertado\n", Console::BOLD);
        }

        $this->stdout("Registros insertados en la tabla producto\n", Console::FG_GREEN);
    }
}